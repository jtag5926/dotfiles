#!/bin/sh
VERSION=2.8
wget https://github.com/tmux/tmux/releases/download/${VERSION}/tmux-${VERSION}.tar.gz
tar xf tmux-${VERSION}.tar.gz
rm -f tmux-${VERSION}.tar.gz
cd tmux-${VERSION}
./configure
make $(nproc)
sudo make install
sudo mv tmux /usr/local/bin/
cd -
rm -rf tmux-${VERSION}
