# dotfiles
Project to store configuration files which will enhance the terminal experience.

## Requirements
* Tmux
* oh-my-zsh
* Nerd Font (Inconsolata)
* Neovim (init.nvim goes in ~/.config/nvim/ then run :PlugInstall to install all plugins)

## Optional
* Pixel-saver (gnome extension)
* Dracula theme for gnome-terminal

